import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

//Context
import {CurrentUserProvider} from "./Functions/GetUser.context";

//Components
import Login from './Components/GetUser/Login';
import Register from './Components/GetUser/Register';

import DashBoard from './Components/DashBoard';

class App extends Component {
    render() {
        return (
            <CurrentUserProvider>
                <React.Fragment>
                    <Switch>
                        <Route exact path='/' component={Login}/>
                        <Route path='/register' component={Register}/>
                        <Route path='/profile' component={DashBoard}/>
                    </Switch>
                </React.Fragment>
            </CurrentUserProvider>
        );
    }
}

export default App;
