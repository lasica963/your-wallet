import React, {Component} from 'react';
import AddTransactionPop from './AddTransactionPop';

import {PopContextConsumer} from "../Functions/Pop.context";

export default class AddTransaction extends Component {
    render() {
        return (
            <React.Fragment>
                <PopContextConsumer>
                    {
                        value => {
                            return (
                                <React.Fragment>
                                    <div className='fix bottom20 right20 btn btn--green btn--add--trans'
                                         onClick={value.openPopup}>
                                        +
                                    </div>
                                    <AddTransactionPop show={value.isOpen} onClose={value.closePopup}/>
                                </React.Fragment>
                            )
                        }
                    }
                </PopContextConsumer>
            </React.Fragment>
        )
    }
}
