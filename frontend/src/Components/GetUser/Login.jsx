import React, {Component} from 'react';
import {CurrentUserConsumer} from "../../Functions/GetUser.context";
import {Link} from "react-router-dom";

export default class LoginBox extends Component {
    render() {
        return (
            <CurrentUserConsumer>
                {value => {
                    const {onChange, submitLogin} = value;

                    return (
                        <main className='bg--dark--gray flex align-center justify-center'>
                            <div className='login-box padt30 padb30 padr30 padl30'>
                                <h1 className='txt-center'>User panel</h1>
                                <span className="xstiny bl mt10 txt-center mb15">Walcome in YourWallet App! <br/> Sign in to manage your home finance.</span>
                                <hr/>
                                <label htmlFor="email" className="inp mt25">
                                    <input type="text" name='email' id="email" placeholder="&nbsp;" onChange={onChange('email')}/>
                                        <span className="label">E-mail</span>
                                        <span className="border">&nbsp;</span>
                                </label>
                                <label htmlFor="pass" className="inp mt25">
                                    <input type="text" name='password' id="pass" placeholder="&nbsp;" onChange={onChange('password')}/>
                                    <span className="label">Password</span>
                                    <span className="border">&nbsp;</span>
                                </label>
                                <div className="row mt30">
                                    <button className="btn btn--blue fw600 padt10 padb10 padr15 padl15" onClick={
                                        (e) => submitLogin(e).then(
                                            (res) => {
                                                if (res) {
                                                    this.props.history.push('/profile');
                                                }
                                            }
                                        )
                                    }>
                                        Login
                                    </button>

                                    <Link to='/register' className='bl row mt20 tiny'>
                                        Don't have account? Register now!
                                    </Link>
                                </div>
                            </div>
                        </main>
                    )
                }}
            </CurrentUserConsumer>
        )
    }
}