import React, {Component} from 'react';
import {CurrentUserConsumer} from "../../Functions/GetUser.context";
import {Link} from "react-router-dom";

export default class Register extends Component {
    render() {
        return (
            <CurrentUserConsumer>
                {
                    value => {
                        const {onChange, submitRegister} = value;

                        return (
                            <main className='bg--dark--gray flex align-center justify-center'>
                                <div className='login-box bg--white padt30 padb30 padr30 padl30'>
                                    <h1>Registration</h1>
                                    <label htmlFor="name" className="inp mt25">
                                        <input type="text" name='name' id="name" placeholder="&nbsp;"
                                               onChange={onChange('name')}/>
                                        <span className="label">Name</span>
                                        <span className="border">&nbsp;</span>
                                    </label>
                                    <label htmlFor="email" className="inp mt25">
                                        <input type="text" name='email' id="email" placeholder="&nbsp;"
                                               onChange={onChange('email')}/>
                                        <span className="label">E-mail</span>
                                        <span className="border">&nbsp;</span>
                                    </label>
                                    <label htmlFor="pass" className="inp mt25">
                                        <input type="text" name='password' id="pass" placeholder="&nbsp;"
                                               onChange={onChange('password')}/>
                                        <span className="label">Password</span>
                                        <span className="border">&nbsp;</span>
                                    </label>
                                    <div className="row mt20">
                                        <button className="btn btn--blue padt10 padb10 padr15 padl15"
                                                onClick={(e) => submitRegister(e).then(() => {
                                                    this.props.history.push('/');
                                                })}>
                                            Register
                                        </button>

                                        <Link to='/' className='bl row mt20 tiny'>
                                            I have an account
                                        </Link>
                                    </div>
                                </div>
                            </main>

                        )
                    }
                }
            </CurrentUserConsumer>
        )
    }
}