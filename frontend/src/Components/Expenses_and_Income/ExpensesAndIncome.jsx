import React, {Component} from 'react';
import {Line} from "react-chartjs-2";

export default class ExpensesAndIncome extends Component {
    render() {
        const data= {
            labels: [" ", " ", " ", " ", " ", " ", " "],
            datasets: [{
                label: "Expense",
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: [0, 10, 5, 2, 20, 30, 45],
            }]
        };
        return (
            <div className="col-white">
                <h1 className="title">Expenses and Income</h1>
                <div className="row padr30 padl30 mt25">
                    <Line data={data}/>
                </div>
            </div>
        )
    }
}