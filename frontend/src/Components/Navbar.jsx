import React, {Component} from 'react';
import {Link} from 'react-router-dom';


export default class Navbar extends Component {
    render() {
        return (
            <div className='row main-manu fix bottom0 left0'>
                <div className="container auto flex justify-center align-center">
                    <div>
                        <Link to="/">
                            Login
                        </Link>
                        <Link to="/register">
                            Register
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}