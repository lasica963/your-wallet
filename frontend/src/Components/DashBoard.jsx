import React, {Component} from 'react';
import CurrentWallet from './CurrentWallet';
import AddTransaction from './AddTransaction'
import Menu from "./Menu";
import TopBar from "./TopBar";
import MyCard from './MyCard';
import SpendingCard from "./SpendingCard";
import PaymentAnalysis from "./PaymentAnalysis/PaymentAnalysis";
import ExpensesAndIncome from "./Expenses_and_Income/ExpensesAndIncome";
import TransactionList from './TransactionList/TransactionList';

import {PopContextProvider} from "../Functions/Pop.context";
import {TransactionProvider} from "../Functions/Transactions.context";

export default class DashBoard extends Component {
    render() {
        return (
            <React.Fragment>
                <TransactionProvider>
                    <PopContextProvider>

                        <Menu/>

                        <TopBar/>

                        <div className='main-content'>
                            <div className='grid-box balance-grid'>
                                <CurrentWallet/>
                            </div>
                            <div className='grid-box my-card-grid'>
                                <MyCard/>
                            </div>
                            <div className='grid-box spending-card-grid'>
                                <SpendingCard/>
                            </div>
                            <div className='grid-box payment-analysis-grid'>
                                <PaymentAnalysis/>
                            </div>
                            <div className='grid-box chart-grid'>
                                <ExpensesAndIncome/>
                            </div>
                            <div className='grid-box transaction-grid'>
                                <TransactionList/>
                            </div>
                            {/*<div className='grid-box'>*/}
                                {/*<ExpensesAndIncome/>*/}
                            {/*</div>*/}
                            {/*<div className='grid-box'>*/}
                                {/*<CurrentWallet/>*/}
                            {/*</div>*/}
                            {/*<div className='grid-box'>*/}
                                {/*<CurrentWallet/>*/}
                            {/*</div>*/}
                            {/*<div className='grid-box'>*/}
                                {/*<CurrentWallet/>*/}
                            {/*</div>*/}
                            {/*<HowManySpent/>*/}

                            {/*<TransactionList/>*/}

                            <AddTransaction/>
                        </div>

                    </PopContextProvider>
                </TransactionProvider>
            </React.Fragment>
        )
    }

}