import React, {Component} from 'react';
import {TransactionConsumer} from "../Functions/Transactions.context";

export default class HowManySpent extends Component {
    render() {
        return (
            <TransactionConsumer>
                {
                    value => {
                        return (
                            <div className='container auto flex flex-wrap mt40 mb40'>
                                <div className="col-6 flex justify-center">
                                    <div>
                                    <span className='xstiny'>
                                        SPENT THIS WEEK
                                    </span>
                                        <h6 className='title txt-center'>{value.spentWeek}$</h6>
                                    </div>
                                </div>
                                <div className="col-6 flex justify-center">
                                    <div>
                                        <span className='xstiny'>
                                            SPENT THIS MOUNTH
                                        </span>
                                        <h6 className='title txt-center'>{value.spentMonth}$</h6>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
            </TransactionConsumer>
        )
    }
}