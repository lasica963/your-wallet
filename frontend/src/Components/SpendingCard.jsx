import React, {Component} from 'react';

export default class SpendingCard extends Component {
    render() {
        return (
            <div className=" col-white">
                <h1 className="title">Spending</h1>
                <div className="row padr30 padl30 mt25">
                    <div className="spending-card rel">
                        <span className="small abs left30 fix-y mt-30">
                            $4153.93
                        </span>
                    </div>
                </div>
            </div>
        )
    }

}