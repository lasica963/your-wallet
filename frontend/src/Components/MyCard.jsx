import React, {Component} from 'react';
import Visa from "../img/visa-logo.png";


export default class MyCard extends Component {
    render() {
        return (
            <div className=" col-white">
                <h1 className="title">My Card</h1>
                <div className="row padr30 padl30 mt25">
                    <div className="my-card rel">

                        <img src={Visa} alt="" className="abs top30 left20"/>

                        <span className="bl row abs fix-y txt-center small">
                            * * * * &nbsp; * * * * &nbsp; * * * * &nbsp; 5 9 7 8
                        </span>

                        <div className="flex flex-wrap abs bottom20 left20 fw300">
                            <span className="xstiny2 uppercase bl row">
                                card holder
                            </span>
                            <span className="fw400">Łukasz Anonim</span>
                        </div>
                        <div className="flex flex-wrap abs bottom20 right20 fw300 txt-right">
                            <span className="xstiny2 uppercase bl row">
                                valid thru
                            </span>
                            <span className="fw400 bl row">07 / 26</span>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}