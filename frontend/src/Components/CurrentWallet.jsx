import React, {Component} from 'react';
import {TransactionConsumer} from "../Functions/Transactions.context";


export default class CurrentWallet extends Component {
    render() {
        return (
            <TransactionConsumer>
                {
                    value => {
                        return (
                            <React.Fragment>
                                <div className="col-white">
                                    <h1 className="title">Balance</h1>
                                    <div className="row flex flex-wrap justify-center txt-center mt50">
                                        <h1 className="row tiny2 mb10">{value.bankAccount} $</h1>
                                        <span className="xstiny2">Current balance</span>
                                    </div>
                                    <div className="row mt30 flex flex-wrap justify-center mr10">
                                        <div className="txt-center spent-box">
                                            <span className="xstiny2 fw700">SPENT THIS WEEK</span>
                                            <h6 className="xstiny fw400">{value.spentWeek} $</h6>
                                        </div>
                                        <div className="txt-center spent-box ml10">
                                            <span className="xstiny2 fw700">SPENT THIS MOUNTH</span>
                                            <h6 className="xstiny fw400">{value.spentMonth} $</h6>
                                        </div>
                                    </div>
                                </div>
                            </React.Fragment>
                        )
                    }
                }

            </TransactionConsumer>
        )
    }

}