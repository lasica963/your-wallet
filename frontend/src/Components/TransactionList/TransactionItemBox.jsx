import React from 'react';
import {TransactionConsumer} from "../../Functions/Transactions.context";
import eye from '../../img/eye.png';
import TransactionDetailPop from "./TransactionDetailPop";
import {PopContextConsumer, PopContextProvider} from "../../Functions/Pop.context";

export default function TransactionItemBox({item}) {
    return (
        <TransactionConsumer>
            {
                value => {
                    return (

                        <div className='row transaction-item rel'>
                            <div className="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14.625" height="18"
                                     viewBox="0 0 14.625 18">
                                    <path id="ico"
                                          d="M0-15.223a33.476,33.476,0,0,1,.562,4.535,4.027,4.027,0,0,1-.65,2.3A3.61,3.61,0,0,1-1.863-7l.457,8.367a.732.732,0,0,1-.229.615.843.843,0,0,1-.615.264H-4.5a.843.843,0,0,1-.615-.264.732.732,0,0,1-.229-.615L-4.887-7A3.61,3.61,0,0,1-6.662-8.385a4.027,4.027,0,0,1-.65-2.3,33.476,33.476,0,0,1,.562-4.535.641.641,0,0,1,.457-.457,1.147,1.147,0,0,1,.791,0,.574.574,0,0,1,.439.492v4.957q.035.105.281.105t.281-.07l.281-4.992a.647.647,0,0,1,.457-.492,1.1,1.1,0,0,1,.773,0,.647.647,0,0,1,.457.492L-2.25-10.2q.035.07.281.07t.281-.105v-4.957a.574.574,0,0,1,.439-.492,1.147,1.147,0,0,1,.791,0A.641.641,0,0,1,0-15.223ZM4.184-5.168a6.027,6.027,0,0,1-2.355-3.48,5.405,5.405,0,0,1,.352-3.41A6.913,6.913,0,0,1,4.1-14.7,3.767,3.767,0,0,1,6.469-15.75a.814.814,0,0,1,.6.246.814.814,0,0,1,.246.6V1.406a.814.814,0,0,1-.246.6.814.814,0,0,1-.6.246H4.5a.8.8,0,0,1-.633-.281.806.806,0,0,1-.211-.633Z"
                                          transform="translate(7.313 15.75)" fill="#fff"></path>
                                </svg>
                            </div>
                            <div>
                                <div className="row flex align-end">
                                    <h6 className="title-transaction">{item.title}</h6>
                                </div>
                                <small className="category-name">{item.category}</small>
                            </div>
                            <div>
                                {
                                    (item.type === 'plus') ?
                                        <div>
                                            <div className="row flex">
                                                <span className='tiny2 col-green fw600'>+{item.amount}$</span>
                                            </div>
                                            <small className="category-name">Amount</small>
                                        </div>
                                        :
                                        <div>
                                            <div className="row flex">
                                                <span className='tiny2 col-red fw600'>-{item.amount}$</span>
                                            </div>
                                            <small className="category-name">Amount</small>
                                        </div>
                                }
                            </div>
                            <div>
                                <div className="row flex align-end">
                                    <h6 className="title-transaction">{item.created_at}</h6>
                                </div>
                                <small className="category-name">Date</small>
                            </div>
                            <PopContextProvider>
                                <PopContextConsumer>
                                    {
                                        value => {
                                            return (
                                                <React.Fragment>
                                                    <div>
                                                        <img src={eye} alt=""
                                                             onClick={value.openPopup}
                                                             className="eye-icon"
                                                        />
                                                    </div>
                                                    <TransactionDetailPop show={value.isOpen}
                                                                          onClose={value.closePopup}
                                                                          item={item}
                                                                          value={value}
                                                    />
                                                </React.Fragment>
                                            )
                                        }
                                    }
                                </PopContextConsumer>
                            </PopContextProvider>
                        </div>
                    )
                }
            }

        </TransactionConsumer>

    )
}