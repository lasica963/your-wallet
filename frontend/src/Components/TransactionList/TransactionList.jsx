import React, {Component} from 'react';
import {TransactionConsumer} from "../../Functions/Transactions.context";
import ListOfTransaction from './ListOfTransaction';

export default class TransactionList extends Component {
    render() {
        return (
            <div className="col-white">
                <h1 className="title">Transactions</h1>
                <div className="row padr30 padl30">
                    <TransactionConsumer>
                        {
                            value => {
                                return (
                                    <ListOfTransaction value={value.transactionsList}/>
                                )
                            }
                        }
                    </TransactionConsumer>
                </div>
            </div>
        )
    }
}