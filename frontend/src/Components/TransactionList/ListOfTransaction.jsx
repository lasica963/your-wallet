import React from 'react'
import TransactionItemBox from './TransactionItemBox';

export default function CartList({value}) {

    return (
        <div className="row flex flex-wrap">
            {
                value.map(item => {
                    return <TransactionItemBox key={item.id} item={item}/>
                })
            }
        </div>
    )
}