import React from 'react';
import {TransactionConsumer} from "../../Functions/Transactions.context";

class TransactionDetailPop extends React.Component {
    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <TransactionConsumer>
                {
                    value => {
                        const item = this.props.item;

                        return value.InEditingMode === item.id ?
                            <div className="popup flex align-center justify-center">
                                <div className="popup_inner">
                                    <h1 className="title col-white">Transaction detail</h1>
                                    <button className="btn btn--red padt15 padb15 padr15 padl15 abs top0 right0 fw700"
                                            onClick={this.props.onClose}>✖
                                    </button>
                                    <div className='row transaction-item-detail rel padt15 padb15 padr30 padl30'>
                                        <div className="padr15">
                                            <div className="row flex align-end">
                                                <input type="text" className="row" placeholder={item.title}
                                                       onChange={value.onChange('title')}
                                                />
                                            </div>
                                            <small className="category-name">{item.category}</small>
                                        </div>
                                        <div className="padr15">
                                            <div className="row">
                                                <input type="text" placeholder={item.amount}
                                                       onChange={value.onChange('amount')}
                                                />
                                            </div>
                                            <small className="category-name">Amount</small>
                                        </div>
                                        <div>
                                            <div className="row flex align-end">
                                                <h6 className="title-transaction">{item.created_at}</h6>
                                            </div>
                                            <small className="category-name">Date</small>
                                        </div>
                                        <div className="flex justify-center">
                                            <svg onClick={() => {
                                                value.updateTransaction(item.id)
                                            }} width="20" height="20" aria-hidden="true" focusable="false"
                                                 data-prefix="fas"
                                                 data-icon="check" className="mr10 svg-inline--fa fa-check fa-w-16"
                                                 role="img"
                                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                <path fill="#29aba4"
                                                      d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path>
                                            </svg>
                                        </div>
                                        <div className="flex justify-center">
                                            <svg onClick={
                                                value.closeEditingMode
                                            } width="20" height="20" aria-hidden="true" focusable="false"
                                                 data-prefix="fas"
                                                 data-icon="times" className="svg-inline--fa fa-times fa-w-11"
                                                 role="img"
                                                 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512">
                                                <path fill="#eb7260"
                                                      d="M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            :

                            <div className="popup flex align-center justify-center">
                                <div className="popup_inner">
                                    <h1 className="title col-white">Transaction detail</h1>
                                    <button className="btn btn--red padt15 padb15 padr15 padl15 abs top0 right0 fw700"
                                            onClick={this.props.onClose}>✖
                                    </button>
                                    <div className='row transaction-item-detail rel padt15 padb15 padr30 padl30'>
                                        <div>
                                            <div className="row flex align-end">
                                                <h6 className="title-transaction">{item.title}</h6>
                                            </div>
                                            <small className="category-name">{item.category}</small>
                                        </div>
                                        <div>
                                            {
                                                (item.type === 'plus') ?
                                                    <div>
                                                        <div className="row flex">
                                                            <span
                                                                className='tiny2 col-green fw600'>+{item.amount}$</span>
                                                        </div>
                                                        <small className="category-name">Amount</small>
                                                    </div>
                                                    :
                                                    <div>
                                                        <div className="row flex">
                                                            <span className='tiny2 col-red fw600'>-{item.amount}$</span>
                                                        </div>
                                                        <small className="category-name">Amount</small>
                                                    </div>
                                            }
                                        </div>
                                        <div>
                                            <div className="row flex align-end">
                                                <h6 className="title-transaction">{item.created_at}</h6>
                                            </div>
                                            <small className="category-name">Date</small>
                                        </div>
                                        <div className="flex justify-center">
                                            <svg onClick={() => {
                                                value.editTransaction(item.id)
                                            }}
                                                 name="true" aria-hidden="true" width="20" height="20" focusable="false"
                                                 data-prefix="far"
                                                 data-icon="edit"
                                                 className="svg-inline--fa fa-edit fa-w-18" role="img"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 576 512">
                                                <path fill="#29aba4"
                                                      d="M402.3 344.9l32-32c5-5 13.7-1.5 13.7 5.7V464c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V112c0-26.5 21.5-48 48-48h273.5c7.1 0 10.7 8.6 5.7 13.7l-32 32c-1.5 1.5-3.5 2.3-5.7 2.3H48v352h352V350.5c0-2.1.8-4.1 2.3-5.6zm156.6-201.8L296.3 405.7l-90.4 10c-26.2 2.9-48.5-19.2-45.6-45.6l10-90.4L432.9 17.1c22.9-22.9 59.9-22.9 82.7 0l43.2 43.2c22.9 22.9 22.9 60 .1 82.8zM460.1 174L402 115.9 216.2 301.8l-7.3 65.3 65.3-7.3L460.1 174zm64.8-79.7l-43.2-43.2c-4.1-4.1-10.8-4.1-14.8 0L436 82l58.1 58.1 30.9-30.9c4-4.2 4-10.8-.1-14.9z"></path>
                                            </svg>
                                        </div>
                                        <div className="flex justify-center">
                                            <svg onClick={() => {
                                                value.removeTransaction(item.id)
                                            }} aria-hidden="true" width="15" height="20" focusable="false"
                                                 data-prefix="fas"
                                                 data-icon="trash"
                                                 className="svg-inline--fa fa-trash fa-w-14" role="img"
                                                 xmlns="http://www.w3.org/2000/svg"
                                                 viewBox="0 0 448 512">
                                                <path fill="#eb7260"
                                                      d="M432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16zM53.2 467a48 48 0 0 0 47.9 45h245.8a48 48 0 0 0 47.9-45L416 128H32z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    }
                }
            </TransactionConsumer>
        );
    }
}

export default TransactionDetailPop;