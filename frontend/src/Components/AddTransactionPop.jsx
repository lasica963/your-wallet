import React from 'react';
import {TransactionConsumer} from "../Functions/Transactions.context";

class AddTransactionPop extends React.Component {
    render() {
        if (!this.props.show) {
            return null;
        }

        return (
            <TransactionConsumer>
                {
                    value => {
                        const {onChange, sendTransaction, setTypeTransaction} = value;

                        return (

                            <div className="popup flex align-center justify-center">
                                <div className="popup_inner">
                                    <h1 className="title col-white">Add transaction</h1>
                                    <button className="btn btn--red padt15 padb15 padr15 padl15 abs top0 right0 fw700"
                                            onClick={this.props.onClose}>✖
                                    </button>
                                    <div className="row flex flex-wrap padr30 padl30 padt15 padb30">
                                        <input type="text" placeholder='Type title' onChange={onChange('title')}/>
                                        <div className='row flex justify-between mt20'>
                                            <div>
                                                <button className='btn btn--quantitiy mr10' name='plus'
                                                        onClick={setTypeTransaction}>
                                                    +
                                                </button>
                                                <button className='btn btn--quantitiy' name='minus'
                                                        onClick={setTypeTransaction}>
                                                    -
                                                </button>
                                            </div>
                                            <div>
                                                <input type="text" placeholder='Amount' onChange={onChange('amount')}/>
                                            </div>
                                        </div>
                                        <div className='row mt20'>
                                            <select name="category" id="category">
                                                <option value="Shopping">Shopping</option>
                                                <option value="Car">Car</option>
                                                <option value="Dog">Dog</option>
                                                <option value="RTV">RTV</option>
                                                <option value="Bills">Bills</option>
                                            </select>
                                        </div>
                                        <div className="row mt20">
                                            <button className='btn btn--blue fw600 padt10 padb10 padr15 padl15'
                                                    onClick={(e) => sendTransaction(e).then(this.props.onClose)}>
                                                Add
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                }
            </TransactionConsumer>
        );
    }
}

export default AddTransactionPop;