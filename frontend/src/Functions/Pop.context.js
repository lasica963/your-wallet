import React, {Component} from 'react';

const PopContext = React.createContext();

export class PopContextProvider extends Component {
    state = {
        isOpen: false
    };

    openPopup = () => {
        this.setState({
            isOpen: true
        });
    };

    closePopup = () => {
        this.setState({
            isOpen: false
        });
    };

    render() {
        return (
            <PopContext.Provider value={{
                ...this.state,
                openPopup: this.openPopup,
                closePopup: this.closePopup
            }}>
                {this.props.children}
            </PopContext.Provider>
        )
    }
}

export const PopContextConsumer = PopContext.Consumer;
