import React, {Component} from 'react';
import axios from 'axios';

const TransactionContext = React.createContext();

export class TransactionProvider extends Component {
    state = {
        title: '',
        type: '',
        amount: '',
        category: 'categoryName',
        transactionsList: [],
        spentWeek: '',
        spentMonth: '',
        bankAccount: '0',
        InEditingMode: ''
    };

    componentDidMount() {
        this.getTransactionList();
        this.spentCount();
        this.getBankAccount();
    }

    onChange = inputName => e => {
        this.setState(Object.assign({}, this.state, {[inputName]: e.target.value}));
        this.setTransaction();
    };

    setTransaction = inputName => e => {

        this.setState(Object.assign({}, this.state.transactionsList, {[inputName]: e.target.value}));

    };

    setTypeTransaction = e => {
        e.target.classList.contains('act') ? e.target.classList.remove('act') : e.target.classList.add('act');

        this.setState({
            type: e.target.name,
            isInEditingMode: false
        })
    };

    sendTransaction = e => {
        e.preventDefault();

        return (
            axios.post('api/transactions', this.state, {
                headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
            }).then(res => {
                if (res) {
                    this.getTransactionList();
                    this.updateSpentCount(this.state.type === 'minus' ? this.state.amount = parseInt(+this.state.amount) : '');
                    this.setState({
                        bankAccount: res.data
                    });
                }
            }).catch(err => {
                console.log(err);
            })
        )
    };

    getTransactionList = () => {
        axios.get('api/transactions', {
            headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
        }).then(res => {
            let tempTransaction = [];
            res.data.forEach(item => {
                const singleItem = {...item};
                tempTransaction = [...tempTransaction, singleItem]
            });

            this.setState({
                transactionsList: tempTransaction
            });
        }).catch(err => {
            console.log(err);
        })
    };

    spentCount = () => {

        axios.get('api/transactions/countSpent', {
            headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
        }).then(res => {
            if (res) {
                this.setState({
                    spentWeek: res.data.week,
                    spentMonth: res.data.month
                })
            }
        }).catch(err => {
            console.log(err);
        })

    };

    updateSpentCount = value => {

        const tempSpentWeek = parseInt(this.state.spentWeek);
        const tempSpentMonth = parseInt(this.state.spentMonth);
        // const getType = this.state.type;

        this.setState({
            spentWeek: tempSpentWeek + value,
            spentMonth: tempSpentMonth + value
        })

    };

    removeTransaction = id => {
        let tempTransaction = [...this.state.transactionsList];

        tempTransaction = tempTransaction.filter(item => item.id !== id);

        let findDeletedTransaction = this.state.transactionsList.find(item => item.id === id);

        const itemId = {
            itemId: parseInt(id),
            type: findDeletedTransaction.type,
            amount: findDeletedTransaction.amount,
        };

        axios.post('api/transactions/removeTransaction', itemId, {
            headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
        }).then(res => {
            if (res) {
                this.setState({
                    transactionsList: [...tempTransaction],
                    bankAccount: res.data
                });
                this.updateSpentCount(findDeletedTransaction.type === 'minus' ? this.state.amount = parseInt(-findDeletedTransaction.amount) : '');
            }
        }).catch(err => {
            console.log(err);
        });
    };

    getBankAccount = () => {

        axios.get('api/transactions/bankAccount', {
            headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
        }).then(res => {

            this.setState({
                bankAccount: res.data,
            })

        }).catch(err => {
            console.log(err);
        })

    };

    getItem = id => {
        const transaction = this.state.transactionsList.find(item => item.id === id);
        return transaction;
    };


    editTransaction = id => {

        let tempProduct = [...this.state.transactionsList];
        const index = tempProduct.indexOf(this.getItem(id));
        const product = tempProduct[index];

        this.setState({
            InEditingMode: product.id
        });

    };

    closeEditingMode = () => {

        this.setState({
            InEditingMode: ''
        })
    };

    updateTransaction = id => {

        let tempProduct = [...this.state.transactionsList];
        const index = tempProduct.indexOf(this.getItem(id));
        const product = tempProduct[index];

        const updateItem = {
            id: product.id,
            title: this.state.title,
            amount: this.state.amount,
            type: this.state.type
        };

        axios.post('api/transactions/updateTransaction', {updateItem}, {
            headers: {Authorization: `Bearer ${localStorage.getItem('userToken')}`}
        }).then(res => {
            if (res) {
                this.closeEditingMode();
                this.setState({
                    title: res.data.title,
                    amount: res.data.amount
                })
            }
        }).catch(err => {
            console.log(err);
        })

    };


    render() {
        return (
            <TransactionContext.Provider value={{
                ...this.state,
                onChange: this.onChange,
                sendTransaction: this.sendTransaction,
                setTypeTransaction: this.setTypeTransaction,
                toggleClass: this.toggleClass,
                getTransactionList: this.getTransactionList,
                spentCount: this.spentCount,
                removeTransaction: this.removeTransaction,
                getBankAccount: this.getBankAccount,
                editTransaction: this.editTransaction,
                closeEditingMode: this.closeEditingMode,
                updateTransaction: this.updateTransaction
            }}>
                {this.props.children}
            </TransactionContext.Provider>
        )
    }
}

export const TransactionConsumer = TransactionContext.Consumer;

