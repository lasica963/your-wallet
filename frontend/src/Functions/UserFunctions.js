import axios from 'axios';

export const login = user => {
    return axios
        .post('api/login', {
            email: user.email,
            password: user.password
        }, {
            headers: {'Content-Type': 'application/json'}
        }).then(res => {
            localStorage.setItem('userToken', res.data.token);
            console.log(res);
            return res;
        }).catch(err => {
            console.log(err);
        })
};

export const register = newUser => {
    return axios
        .post('api/register', newUser, {
            headers: {'Content-Type': 'application/json'}
        }).then(res => {
            console.log(res);
        }).catch(err => {
            console.log(err);
        })
};

export const getUserProfil = () => {
    return axios
        .post('api/profile', {
            headers: {Authorization: `Test ${localStorage.userToken}`}
        })
        .then(res => {
            // console.log(res);
            return res.data;
        })
        .catch(err => {
            console.log(err);
        })
};