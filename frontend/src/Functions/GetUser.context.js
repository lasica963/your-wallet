import React, {Component} from 'react';
import {login, register} from "./UserFunctions";

const CurrentUserContext = React.createContext();

export class CurrentUserProvider extends Component {
    state = {
        name: '',
        email: '',
        password: '',
        errors: {},
    };

    onChange = inputName => e => {
        this.setState(Object.assign({}, this.state, {[inputName]: e.target.value}));
    };

    submitLogin = e => {
        e.preventDefault();

        const user = {
            email: this.state.email,
            password: this.state.password
        };

        return login(user);
    };

    submitRegister = e => {
        e.preventDefault();

        const newUser = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        };

        return register(newUser);

    };

    render() {
        const {children} = this.props;
        return (
            <CurrentUserContext.Provider value={{
                ...this.state,
                onChange: this.onChange,
                submitLogin: this.submitLogin,
                submitRegister: this.submitRegister
            }}>
                {children}
            </CurrentUserContext.Provider>
        );
    }
}

export const CurrentUserConsumer = CurrentUserContext.Consumer;
