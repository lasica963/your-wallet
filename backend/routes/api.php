<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function () {
    Route::apiResource('transactions', 'TransactionController')->only([
        'store', 'index'
    ]);
    Route::get('transactions/countSpent', 'TransactionController@countSpent');
    Route::get('transactions/bankAccount', 'BankAccountController@index');
    Route::post('transactions/removeTransaction', 'TransactionController@removeTransaction');
    Route::post('transactions/updateTransaction', 'TransactionController@updateTransaction');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('profile', 'UserController@getAuthenticatedUser');

});