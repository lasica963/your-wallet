<?php

namespace App\Http\Controllers;

use App\BankAccount;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    public function index()
    {
        $total = BankAccount::where('user_id', '=', auth()->user()->id)->firstOrFail()->total_cash;
        return response($total);
    }
}
