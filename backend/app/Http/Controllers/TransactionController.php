<?php


namespace App\Http\Controllers;

use App\BankAccount;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Carbon;

class TransactionController extends Controller
{
    public function index()
    {
        $transaction = Transaction::where('user_id', '=', auth()->user()->id)->get();
        return response()->json($transaction);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'type' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'category' => 'required|string|max:255',
            'amount' => 'required|integer',
        ]);

        $request->merge([
            'user_id' => auth()->user()->id,
        ]);

        $totalCash = BankAccount::where('user_id', '=', auth()->user()->id)->firstOrFail()->total_cash;

        if ($request->type === 'minus') {
            $update = BankAccount::where('user_id', '=', auth()->user()->id)->update(['total_cash' => $totalCash - $request->amount]);
            $sum = $totalCash - $request->amount;

        } else {
            $update = BankAccount::where('user_id', '=', auth()->user()->id)->update(['total_cash' => $totalCash + $request->amount]);
            $sum = $totalCash + $request->amount;
        }

        if (!Transaction::create($request->all())) {
            return response(null, 400);
        }

        return response($sum, 201);

    }

    public function countSpent()
    {
        return response()->json([
            'week' => Transaction::where('created_at', '>=', now()->startOfWeek())->where('type', '=', 'minus')->sum('amount'),
            'month' => Transaction::where('created_at', '>=', now()->startOfMonth())->where('type', '=', 'minus')->sum('amount'),
        ]);
    }

    public function removeTransaction(Request $request)
    {
        $item = $request->json('itemId');
        $transaction = Transaction::where('id', $item)->delete();

        $totalCash = BankAccount::where('user_id', '=', auth()->user()->id)->firstOrFail()->total_cash;

        if ($request->type === 'minus') {
            $update = BankAccount::where('user_id', '=', auth()->user()->id)->update(['total_cash' => $totalCash + $request->amount]);
            $sum = $totalCash + $request->amount;

        } else {
            $update = BankAccount::where('user_id', '=', auth()->user()->id)->update(['total_cash' => $totalCash - $request->amount]);
            $sum = $totalCash - $request->amount;
        }

        return response($sum, 200);
    }

    public function updateTransaction(Request $request)
    {
        $itemId = $request->updateItem['id'];
        
        $updateDetail = [
            'title' => $request->updateItem['title'],
            'amount' => $request->updateItem['amount']
        ];

        $transaction = Transaction::find($itemId)->update($updateDetail);

        return response($updateDetail, 200);
    }

}
