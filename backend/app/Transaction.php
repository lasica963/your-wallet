<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['id', 'user_id', 'type', 'title', 'category', 'amount'];

//    public function getCreatedAtTextAttribute()
//    {
//        return $this->created_at->format('M d Y');
//    }
//
//    protected $appends = ['CreatedAtText'];
}
